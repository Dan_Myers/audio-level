/*
 * File:   main.c
 * Author: Dan
 *
 */

// PIC18F45K42 Configuration Bit Settings

// 'C' source line config statements

// CONFIG1L
#pragma config FEXTOSC  = OFF               // External Oscillator Selection (Oscillator not enabled)
#pragma config RSTOSC   = HFINTOSC_64MHZ    // Reset Oscillator Selection (HFINTOSC with HFFRQ = 64 MHz and CDIV = 1:1)

// CONFIG1H
#pragma config CLKOUTEN = OFF               // Clock out Enable bit (CLKOUT function is disabled)
#pragma config PR1WAY   = OFF               // PRLOCKED One-Way Set Enable bit (PRLOCK bit can be set and cleared repeatedly)
#pragma config CSWEN    = OFF               // Clock Switch Enable bit (The NOSC and NDIV bits cannot be changed by user software)
#pragma config FCMEN    = OFF               // Fail-Safe Clock Monitor Enable bit (Fail-Safe Clock Monitor disabled)

// CONFIG2L
#pragma config MCLRE    = EXTMCLR           // MCLR Enable bit (If LVP = 0, MCLR pin is MCLR; If LVP = 1, RE3 pin function is MCLR )
#pragma config PWRTS    = PWRT_OFF          // Power-up timer selection bits (PWRT is disabled)
#pragma config MVECEN   = OFF               // Multi-vector enable bit (Interrupt contoller does not use vector table to prioritze interrupts)
#pragma config IVT1WAY  = OFF               // IVTLOCK bit One-way set enable bit (IVTLOCK bit can be cleared and set repeatedly)
#pragma config LPBOREN  = OFF               // Low Power BOR Enable bit (ULPBOR disabled)
#pragma config BOREN    = OFF               // Brown-out Reset Enable bits (Brown-out Reset disabled)

// CONFIG2H
#pragma config BORV     = VBOR_2P45         // Brown-out Reset Voltage Selection bits (Brown-out Reset Voltage (VBOR) set to 2.45V)
#pragma config ZCD      = OFF               // ZCD Disable bit (ZCD disabled. ZCD can be enabled by setting the ZCDSEN bit of ZCDCON)
#pragma config PPS1WAY  = OFF               // PPSLOCK bit One-Way Set Enable bit (PPSLOCK bit can be set and cleared repeatedly (subject to the unlock sequence))
#pragma config STVREN   = OFF               // Stack Full/Underflow Reset Enable bit (Stack full/underflow will not cause Reset)
#pragma config DEBUG    = OFF               // Debugger Enable bit (Background debugger disabled)
#pragma config XINST    = OFF               // Extended Instruction Set Enable bit (Extended Instruction Set and Indexed Addressing Mode disabled)

// CONFIG3L
#pragma config WDTCPS   = WDTCPS_31         // WDT Period selection bits (Divider ratio 1:65536; software control of WDTPS)
#pragma config WDTE     = OFF               // WDT operating mode (WDT Disabled; SWDTEN is ignored)

// CONFIG3H
#pragma config WDTCWS   = WDTCWS_7          // WDT Window Select bits (window always open (100%); software control; keyed access not required)
#pragma config WDTCCS   = SC                // WDT input clock selector (Software Control)

// CONFIG4L
#pragma config BBSIZE   = BBSIZE_512        // Boot Block Size selection bits (Boot Block size is 512 words)
#pragma config BBEN     = OFF               // Boot Block enable bit (Boot block disabled)
#pragma config SAFEN    = OFF               // Storage Area Flash enable bit (SAF disabled)
#pragma config WRTAPP   = OFF               // Application Block write protection bit (Application Block not write protected)

// CONFIG4H
#pragma config WRTB     = OFF               // Configuration Register Write Protection bit (Configuration registers (300000-30000Bh) not write-protected)
#pragma config WRTC     = OFF               // Boot Block Write Protection bit (Boot Block (000000-0007FFh) not write-protected)
#pragma config WRTD     = OFF               // Data EEPROM Write Protection bit (Data EEPROM not write-protected)
#pragma config WRTSAF   = OFF               // SAF Write protection bit (SAF not Write Protected)
#pragma config LVP      = OFF               // Low Voltage Programming Enable bit (HV on MCLR/VPP must be used for programming)

// CONFIG5L
#pragma config CP       = OFF               // PFM and Data EEPROM Code Protection bit (PFM and Data EEPROM code protection disabled)

// CONFIG5H

// #pragma config statements should precede project file includes.
// Use project enums instead of #define for ON and OFF.

// Include xc.h for PIC compiler
#include <xc.h>
// Include stdint.h for fixed size integers
#include <stdint.h>

// Set oscillator frequency for delays
#define _XTAL_FREQ 64000000

// Define constants
#define AUDIO_ADC_CHANNEL   0x00
#define VOLUME_ADC_CHANNEL  0x02
#define MAX_LEDS            10
#define MAX_SEVEN_SEG       99
#define MAX_DAC             0x1f
#define MAX_ADC             4095
#define MAX_GAIN            4
#define MAX_MODE            3
#define MODE_ADDRESS        0x00
#define SLEEP_THRESHOLD     0xff

// Function prototypes
void system_init();
uint16_t getADC(uint8_t);
void LEDbar(uint8_t dot);
void sevenSegment();
void analogueOutput();
double getVolumeMultiplier();
void __interrupt() ISR_ModeChange();
uint8_t EEPROMread(uint8_t address);
void EEPROMwrite(uint8_t address, uint8_t data);

// Global variables
uint8_t mode = 0;
uint16_t sleepCounter = 0;

// Main function
int main(int argc, char* argv[])
{
    // Initialise system
    system_init();
    
    // Infinite loop
    while (1)
    {
        // If mode not using 7 segments
        if (mode < 2)
        {
            // Use LEDs
            LEDbar(mode);
            // Turn 7 segments off
            LATC = 0x00;
            LATD = 0x00;
        }
        // If mode is 7 segments
        else if (mode == 2)
        {
            // Use 7 segment displays
            sevenSegment();
            // Turn LEDs off
            LATA = 0x00;
            LATB = 0x00;
        }
        else
        {
            // Use 7 segment displays
            analogueOutput();
            // Turn LEDs off
            LATA = 0x00;
            LATB = 0x00;
            // Turn 7 segments off
            LATC = 0x00;
            LATD = 0x00;
        }
        __delay_ms(10);
    }
    
    return 0;
}

// Initialise the PIC I/O and ADC
void system_init()
{
    // Delay to prevent mode change on initial power up
    __delay_ms(100);
    
    // Initialise port A, B, C, and D pins as I/O
    TRISA = 0x05;
    TRISB = 0x01;
    TRISC = 0x00;
    TRISD = 0x00;
    
    // Initialise ADC
    ADCON0bits.ADFM     = 1;    // right justify
    ADCON0bits.ADCS     = 0;    // FRC clock
    ADCLK               = 4;    // ADC clock divider
    ANSELAbits.ANSELA0  = 1;    // Set RA0 to analogue
    ANSELAbits.ANSELA2  = 1;    // Set RA0 to analogue
    ADCON0bits.ADON     = 1;    // Turn ADC On
    
    // Initialise interrupt
    ANSELBbits.ANSELB0  = 0;    // Set RB0 to digital
    INTCON0bits.GIEH    = 1;    // High priority interrupts enable
    INTCON0bits.IPEN    = 1;    // Enable priority on interrupts
    INTCON0bits.INT0EDG = 1;    // Interrupt on rising edge of INT0
    PIR1bits.INT0IF     = 0;    // Clear interrupt flag 0
    PIE1bits.INT0IE     = 1;    // Enable external interrupt 0
    IPR1bits.INT0IP     = 1;    // External interrupt 0 high priority
    
    // Initialise DAC
    DAC1CON0bits.EN     = 1;    // Enable DAC
    DAC1CON0bits.OE1    = 0;    // Disable DAC output on DAC1OUT1 pin
    DAC1CON0bits.OE2    = 1;    // Enable DAC output on DAC1OUT2 pin
    DAC1CON0bits.PSS    = 0;    // Use Vdd as positive source
    DAC1CON0bits.NSS    = 0;    // Use Vss as negative source    
    ANSELBbits.ANSELB7  = 1;    // Set RB7 to analogue
    
    // Read mode from EEPROM
    mode = EEPROMread(MODE_ADDRESS);
}

// Get ADC value of specified channel
uint16_t getADC(uint8_t channel)
{
    uint8_t ADC_VALUE_HIGH, ADC_VALUE_LOW;
    // Select ADC channel 0
    ADPCH = channel;
    ADCON0bits.ADGO = 1;
    // Wait for conversion
    while(ADCON0bits.GO);
    ADC_VALUE_HIGH = ADRESH;
    ADC_VALUE_LOW = ADRESL;
    // Get ADC value
    uint16_t adcValue = (ADC_VALUE_HIGH << 8) | ADC_VALUE_LOW;
    
    // If no input sound
    if (channel == AUDIO_ADC_CHANNEL && adcValue <= SLEEP_THRESHOLD)
    {
        // Increment sleep counter
        sleepCounter++;
        // If 30 seconds have passed
        if (sleepCounter >= 3000)
        {
            // Reset counter
            sleepCounter = 0;
            // Turn LEDs off
            LATA = 0x00;
            LATB = 0x00;
            // Turn 7 segments off
            LATC = 0x00;
            LATD = 0x00;
            // Sleep mode
            SLEEP();
        }
    }
    
    // If there is input sound
    else if (channel == AUDIO_ADC_CHANNEL && adcValue > SLEEP_THRESHOLD)
    {
        // Reset sleep counter
        sleepCounter = 0;
    }
    
    // Return ADC value
    return adcValue;
}

// Turn on LED bar, parameter as 1 to show dot instead of bar 
void LEDbar(uint8_t dot)
{
    // Arrays for LED bar states
    const uint8_t LEDbarA[MAX_LEDS + 1] = {0x00, 0x08, 0x18, 0x38, 0x78, 0xf8, 0xf8, 0xf8, 0xf8, 0xf8, 0xf8};
    const uint8_t LEDbarB[MAX_LEDS + 1] = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0x06, 0x0e, 0x1e, 0x3e};
    
    // Calculate number of LEDs to turn on
    uint8_t led_count = (getADC(AUDIO_ADC_CHANNEL) * getVolumeMultiplier()) / (MAX_ADC / MAX_LEDS); 
    
    // If number of LEDs exceeds 10, set to 10
    led_count = (led_count > MAX_LEDS) ? MAX_LEDS : led_count;
    
    // Turn on relevant LEDs, XOR with previous to show dot
    LATA = (led_count > 0 && dot == 1) ? LEDbarA[led_count] ^ LEDbarA[led_count - 1] : LEDbarA[led_count];
    LATB = (led_count > 0 && dot == 1) ? LEDbarB[led_count] ^ LEDbarB[led_count - 1] : LEDbarB[led_count];
}

// Turn on 7 segment displays
void sevenSegment()
{
    // Array for bit patterns to display numbers 0-9
    const uint8_t sevenSegments[10] = {0x3f, 0x06, 0x5b, 0x4f, 0x66, 0x6d, 0x7d, 0x07, 0x7f, 0x6f};
    
    // Calculate number to display on 7 segments
    uint8_t sevenSegNumber = (getADC(AUDIO_ADC_CHANNEL) * getVolumeMultiplier()) / (MAX_ADC / MAX_SEVEN_SEG); 
    
    // If number exceeds 99, set to 99
    sevenSegNumber = (sevenSegNumber > MAX_SEVEN_SEG) ? MAX_SEVEN_SEG : sevenSegNumber;
    
    // Find first digit
    uint8_t segOne = sevenSegNumber / 10;
    // Find second digit
    uint8_t segTwo = sevenSegNumber % 10;
    
    // Turn on relevant segments
    LATC = sevenSegments[segOne];
    LATD = sevenSegments[segTwo];
}

// Use DAC for analogue output
void analogueOutput()
{
    // Calculate value to output through DAC
    uint8_t DACoutput = (getADC(AUDIO_ADC_CHANNEL) * getVolumeMultiplier()) / (MAX_ADC / MAX_DAC); 
    
    // If value exceeds 0x1f, set to 0x1f
    DACoutput = (DACoutput > MAX_DAC) ? MAX_DAC : DACoutput;
    DAC1CON1 = DACoutput;
}

// Calculate volume multiplier
double getVolumeMultiplier()
{
    return (double)getADC(VOLUME_ADC_CHANNEL) / (MAX_ADC / MAX_GAIN);
}

// Mode change interrupt function
void __interrupt() ISR_ModeChange()
{
    // Increase mode number, or set back to 0
    mode = (mode == MAX_MODE) ? 0 : mode + 1;
    // Write current mode to EEPROM
    EEPROMwrite(MODE_ADDRESS, mode);
    // Clear interrupt flag 0
    PIR1bits.INT0IF = 0;
}

// Read EEPROM and return data at passed address
uint8_t EEPROMread(uint8_t address)
{
    NVMCON1             = 0x00;     // Clear register for nonvolatile memory control
    NVMCON1bits.REG     = 0b00;     // Access data EEPROM memory locations
    NVMADRL             = address;  // Data EEPROM memory address
    NVMCON1bits.RD      = 1;        // Enable read control bit
    return NVMDAT;                  // Return data at read from the address
}

// Write data to specified address in EEPROM
void EEPROMwrite(uint8_t address, uint8_t data)
{
    NVMCON1             = 0x00;     // Clear register for nonvolatile memory control
    NVMCON1bits.REG     = 0b00;     // Access data EEPROM memory locations
    NVMADRL             = address;  // Data EEPROM memory address
    NVMDAT              = data;     // Data EEPROM memory data
    NVMCON1bits.WREN    = 1;        // Allows program/erase and refresh cycles
    INTCON0bits.GIE     = 0;        // Disable global interrupts
    NVMCON2             = 0x55;     // NVM UNLOCK SEQUENCE
    NVMCON2             = 0xAA;     // NVM UNLOCK SEQUENCE
    NVMCON1bits.WR      = 1;        // Enable write control bit
    INTCON0bits.GIE     = 1;        // Enable global interrupts
    NVMCON1bits.WREN    = 0;        // Inhibits programming/erasing and user refresh of NVM
}